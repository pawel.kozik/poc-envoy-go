up:
	docker-compose up

down:
	docker-compose down

test:
	curl -v localhost:10000 2>&1

build:
	docker-compose -f docker-compose.build.yaml run --rm go_plugin_compile

.PHONY: up down clean build test
